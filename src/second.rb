def count_spector_two(data, offset = 1)
  old_offset = -1
  while offset < data.length && old_offset != offset
    old_offset = offset
    dna = {}
    (offset...data.length).each do |i|
      current_symbol = data[i]
      code_for_symbol = data[(i - offset)...i]
      dna[code_for_symbol] = current_symbol
      if dna[code_for_symbol].nil?
        if dna[code_for_symbol] != current_symbol
          puts "Мы прошли до #{i} символа, порядок #{offset}"
          offset += 1
          break
        end
      end
    end
    puts 'Мы прошли до последнего символа'
    puts "Порядок #{offset}"
  end
  filename = ARGV[0]
  data = File.read(filename).gsub(/[^ATGC]/, '')[0..100000]
  puts "По первому спектру порядок #{count_spectrum(data)}"
  puts "#{count_spector_two(data)}"
end
