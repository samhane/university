require 'json'

@replacements = {
    'A' => '1',
    'T' => '2',
    'G' => '3',
    'C' => '4'
}

@spectrum_0_pathogen = []
@spectrum_0_non_pathogen = []
@spectrum_0_pathogen_estimates = []
@spectrum_0_non_pathogen_estimates = []

@spectrum_1_pathogen = []
@spectrum_1_non_pathogen = []

@spectrum_2_pathogen = []
@spectrum_2_non_pathogen = []

@pathogen = {}
@non_pathogen = {}

def comb(elements, count)
  translated_elements = []
  elements.each do |element|
    translated_elements << @replacements[element]
  end
  translated_elements.join.chars.map(&:to_i).combination(count).to_a.map(&:join)
end

def check_recurrent(needed, elements)
  available_operations = %w(+ - /)
  if elements.length >= 1
    if elements.include? needed
      return elements.length
    end
    (available_operations * (elements.length - 1)).combination(elements.length - 1).to_a.uniq.each do |operations|
      result = ''
      elements.each_char.with_index do |next_element, index|
        answer = instance_eval("#{result}#{@replacements[next_element]}")
        result = "#{answer}"
        return elements.length if result == @replacements[needed]
        result += "#{operations[index]}" unless operations[index].nil?
      end
    end
  end
  false
end

def compute(current_nucleotide, data, index, estimated_m)
  current_estimated = estimated_m

  loop do
    start_offset = index - current_estimated
    end_offset = index - 1

    if current_estimated > 0
      data_for_check = data[start_offset..end_offset]
      data_for_check = [data_for_check]

      if data_for_check.length > estimated_m
        tmp = data_for_check
        data_for_check = comb(data_for_check, estimated_m)
        data_for_check << tmp
      end
    else
      data_for_check = data[0..end_offset]
      data_for_check = [data_for_check]
    end

    data_for_check.each do |elements|
      recurrent = check_recurrent(current_nucleotide, elements)
      if recurrent
        return recurrent
      end
    end
    return -1 if ((current_estimated += 1) >= (data.length / 2))
  end
end

def count_spectrum(bacteria_name, data)
  bacteria_split = bacteria_name.split '/'
  bacteria_type = bacteria_split[0]
  bacteria_name = bacteria_split[1].split('_')[0..1].join ' '
  estimated = {}

  data.each_char.with_index do |current_nucleotide, index|
    next if index < 1

    result = compute(current_nucleotide, data[0..index], index, 1)
    unless result == -1
      if estimated[result].nil?
        estimated[result] = []
      end
      estimated[result] << index
    end
  end

  m_0 = estimated.max { |a, b| a[1].length <=> b[1].length }.first
  local_answer = {
      label: bacteria_name,
      data: m_0
  }
  if bacteria_type == 'NON_PATHOGEN'
    @spectrum_0_non_pathogen << local_answer
  else
    @spectrum_0_pathogen << local_answer
  end

  total = 0
  estimated.each do |key, value|
    total += key
  end
  current_0 = {
      max: estimated.max { |a, b| a[1].length <=> b[1].length }.first,
      min: estimated.min { |a, b| a[1].length <=> b[1].length }.first,
      average: (total.to_f / estimated.size),
  }
  if bacteria_type == 'NON_PATHOGEN'
    @spectrum_0_non_pathogen_estimates << current_0
  else
    @spectrum_0_pathogen_estimates << current_0
  end

  spectrum_1 = {}
  spectrum_2 = {}
  i = 1
  while i <= m_0
    last_length = 0
    j = 1
    while j < estimated[i].length
      if estimated[i][j] - estimated[i][j - 1] > 1
        if spectrum_1[i].nil?
          spectrum_1[i] = []
        end
        spectrum_1[i] << last_length unless last_length == 0
        last_length = 0
        spectrum_2[i] ||= 0
        spectrum_2[i] += 1
      else
        last_length += 1
      end
      j += 1
    end
    i += 1
  end

  spectrum_1.each do |key, sp|
    local_answer = {
        label: bacteria_name,
        data: sp.length
    }
    if bacteria_type == 'NON_PATHOGEN'
      @spectrum_1_non_pathogen << local_answer
    else
      @spectrum_1_pathogen << local_answer
    end
  end
  spectrum_2.each do |key, sp|
    local_answer = {
        label: bacteria_name,
        data: sp
    }
    if bacteria_type == 'NON_PATHOGEN'
      @spectrum_2_non_pathogen << local_answer
    else
      @spectrum_2_pathogen << local_answer
    end
  end
end

def count_estimates(spectrum)
  unless spectrum.empty?
    total = 0
    spectrum.each do |key|
      total += key[:data]
    end
    {
        max: spectrum.max { |a, b| a[:data] <=> b[:data] }[:data],
        min: spectrum.min { |a, b| a[:data] <=> b[:data] }[:data],
        average: (total.to_f / spectrum.size),
    }
  end
end

path = ARGV[0]

Dir.glob(path) do |file_name|
  file = File.open(file_name, 'r')
  count_spectrum(file_name.sub(ARGV[1], ''), file.read.gsub(/^>.*\n/, '').gsub(/\n/, '')[0..ARGV[2]])
end

@non_pathogen[0] = count_estimates(@spectrum_0_non_pathogen)
@pathogen[0] = count_estimates(@spectrum_0_pathogen)

@non_pathogen[1] = count_estimates(@spectrum_1_non_pathogen)
@pathogen[1] = count_estimates(@spectrum_1_non_pathogen)

@non_pathogen[2] = count_estimates(@spectrum_2_non_pathogen)
@pathogen[2] = count_estimates(@spectrum_2_non_pathogen)

File.open("#{ARGV[3]}/0_non_pathogen.log", "w") { |file| file.write(@spectrum_0_non_pathogen.to_json) }
File.open("#{ARGV[3]}/results/0_pathogen.log", "w") { |file| file.write(@spectrum_0_pathogen.to_json) }

File.open("#{ARGV[3]}/results/1_non_pathogen.log", "w") { |file| file.write(@spectrum_1_non_pathogen.to_json) }
File.open("#{ARGV[3]}/results/1_pathogen.log", "w") { |file| file.write(@spectrum_1_pathogen.to_json) }

File.open("#{ARGV[3]}/results/2_non_pathogen.log", "w") { |file| file.write(@spectrum_2_non_pathogen.to_json) }
File.open("#{ARGV[3]}/results/2_pathogen.log", "w") { |file| file.write(@spectrum_2_pathogen.to_json) }

File.open("#{ARGV[3]}/results/estimates_0_non_pathogen.log", "w") { |file| file.write(@non_pathogen[0].to_json) }
File.open("#{ARGV[3]}/results/estimates_0_pathogen.log", "w") { |file| file.write(@pathogen[0].to_json) }

File.open("#{ARGV[3]}/results/estimates_1_non_pathogen.log", "w") { |file| file.write(@non_pathogen[1].to_json) }
File.open("#{ARGV[3]}/results/estimates_1_pathogen.log", "w") { |file| file.write(@pathogen[1].to_json) }

File.open("#{ARGV[3]}/results/estimates_2_non_pathogen.log", "w") { |file| file.write(@non_pathogen[2].to_json) }
File.open("#{ARGV[3]}/results/estimates_2_pathogen.log", "w") { |file| file.write(@pathogen[2].to_json) }
